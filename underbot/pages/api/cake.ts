// cake.ts

import { NextApiRequest, NextApiResponse } from 'next';
import { URLSearchParams } from 'url';


const parseNightbotUser = (userParams: string) => {
    const params = new URLSearchParams(userParams);
  
    return {
        name: params.get('name'),
        displayName: params.get('displayName'),
        provider: params.get('provider'),
        providerId: params.get('providerId'),
        userLevel: params.get('userLevel'),
    };
};

export default async function (req: NextApiRequest, res: NextApiResponse) {
    const user = parseNightbotUser(req.headers['nightbot-user'] as string);
    let target = req.query.target;

    let cakes = [
        'chocolate cake', 
        'mockalate cake', 
        'lemon cake', 
        'carrot cake',
        'poundcake',
        'shortcake',
        'birthday cake',
        'pineapple upside-down cake',
        'fruit cake',
        'vanilla cake',
        'white cake',
        'red velvet cake',
        'wedding cake',
        'angel food cake',
        'devils food cake',
    ];

    let frostings = [
        'chocolate frosting',
        'vanilla frosting',
        'strawberry frosting',
        'buttercream frosting',
        'cream cheese frosting',
        'lemon frosting',

    ];

    let toppings = [
        'rainbow sprinkles',
        'chocolate sprinkles',
        'oreo crumbles',
        'a plastic bride and groom',
        'plastic action figures',
        'a bunch of candles',

    ];

    let cake = cakes[Math.floor(Math.random() * cakes.length)];
    let frosting = frostings[Math.floor(Math.random() * frostings.length)];
    let topping = toppings[Math.floor(Math.random() * toppings.length)];

    let resText = `${user.displayName} bakes ${target} a ${cake} with ${frosting} topped with ${topping}`;

    res
        .status(200)
        .send(
            resText
        );
};
