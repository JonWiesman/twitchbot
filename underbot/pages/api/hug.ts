// hug.ts

import { NextApiRequest, NextApiResponse } from 'next';
import { URLSearchParams } from 'url';


const parseNightbotUser = (userParams: string) => {
    const params = new URLSearchParams(userParams);
  
    return {
        name: params.get('name'),
        displayName: params.get('displayName'),
        provider: params.get('provider'),
        providerId: params.get('providerId'),
        userLevel: params.get('userLevel'),
    };
};

export default async function (req: NextApiRequest, res: NextApiResponse) {
    const user = parseNightbotUser(req.headers['nightbot-user'] as string);
    let target = req.query.target;


    let outcomes = [
        `${user.displayName} gives ${target} a nice, warm, friendly hug.`,
    ];

    let resText = ``;
    let roll = Math.floor(Math.random() * outcomes.length);

    resText += outcomes[roll];

    res
        .status(200)
        .send(
            resText
        );
};
