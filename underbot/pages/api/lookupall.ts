// lookup.ts

import { NextApiRequest, NextApiResponse } from 'next';
import path from 'path';
import { promises as fs} from 'fs';

export default async function (req: NextApiRequest, res: NextApiResponse) {

    let homeGameHistory = [];
    
    let resText = 'Usage: lookupall [name].';

    const jsonDirectory = path.join(process.cwd(), 'json');
    const fileContents = await fs.readFile(jsonDirectory + '/homegamewinners.json', 'utf8');

    
    if(fileContents)
    {
        if(fileContents.length == 0)
        {
            resText = fileContents;
        }
        else
        {
            console.log(`fileContents = ${fileContents.substring(0, 100)}`);
            let doc = JSON.parse(fileContents);
            console.log(`returned from parsing fileContents!`);
            if(doc && doc.db)
            {
                homeGameHistory = doc.db;
            }
        }
    }

    if(req.query.target)
    {
        let target = req.query.target as string;
        if(target[0] == '@')
        {
            // strip the @
            target = target.substring(1);
        }

        fetch(`https://twitchbot-phi.vercel.app/api/getnick?target=${target}`).then(function(response){
            return response.json();
        }).then(function(myjson) {

            let globalSn = '';
            let twitchSn = '';

            if(myjson.db && myjson.db.length > 0)
            {
                globalSn = myjson.db[0].globalName;
                twitchSn = myjson.db[0].twitchName;
            }
            else
            {
                console.log(`myjson = ${myjson}`);
                let xp = JSON.parse(myjson);
                if(xp && xp.db && xp.db.length > 0)
                {
                    globalSn = xp.db[0].globalName;
                    twitchSn = xp.db[0].twitchName;
                }
            }
            
            let key = target.toLowerCase();

            let historyKey = globalSn.length > 0 ? globalSn : target;
            if(historyKey.length > 0)
            {
                let matches = [];

                for(let i = 0; i < homeGameHistory.length; i++)
                {
                    let info = homeGameHistory[i];
                    let win = info.winner.toLowerCase() == historyKey.toLowerCase();
                    if(win)
                    {
                        matches.push(info);
                    }
                }
                resText = JSON.stringify(matches);
            }
            if(globalSn.length >= 0)
            {
                res
                .status(200)
                .send(
                    `${resText}`
                );
            }
            return;
        });
    }
    else
    {
        res
        .status(200)
        .send(
            `${resText}`
        );
    }
};
