// hug.ts

import { NextApiRequest, NextApiResponse } from 'next';
import path from 'path';
import { promises as fs} from 'fs';



export default async function (req: NextApiRequest, res: NextApiResponse) {
    let target = req.query.target as string;

    const jsonDirectory = path.join(process.cwd(), 'json');

    const fileContents = await fs.readFile(jsonDirectory + '/namesdb.json', 'utf8');

    let resText = '{}';
    if(fileContents)
    {
        if(fileContents.length == 0)
        {
            resText = fileContents;
        }
        else
        {
            console.log(`fileContents = ${fileContents.substring(0, 100)}`);
            let doc = JSON.parse(fileContents);
            console.log(`returned from parsing fileContents!`);
            if(doc && doc.db)
            {
                console.log(`doc.db.length = ${doc.db.length}`);
                resText = '{';
                resText += '    "db": [';
                let retCount = 0;
                for(let i = 0; i < doc.db.length - 1; i += 2)
                {
                    let incNick = target === undefined || (doc.db[i].toLowerCase() == target.toLowerCase() || doc.db[i + 1].toLowerCase() == target.toLowerCase());
    
                    if(incNick)
                    {
                        if(retCount > 0)
                        {
                            resText += ',';
                        }
                        resText += `        {"twitchName": "${doc.db[i]}", "globalName": "${doc.db[i + 1]}"}`;
                        retCount++;
                    }
    
                }
                resText += '    ]}';
            }
        }
    }
    res 
        .status(200)
        .json(resText);
};
