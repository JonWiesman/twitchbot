// ping.ts

import { NextApiRequest, NextApiResponse } from 'next';
import { URLSearchParams } from 'url';


const parseNightbotChannel = (channelParams: string) => {
    const params = new URLSearchParams(channelParams);

    return {
        name: params.get('name'),
        displayName: params.get('displayName'),
        provider: params.get('provider'),
        providerId: params.get('providerId')
    };
};

const parseNightbotUser = (userParams: string) => {
    const params = new URLSearchParams(userParams);
  
    return {
        name: params.get('name'),
        displayName: params.get('displayName'),
        provider: params.get('provider'),
        providerId: params.get('providerId'),
        userLevel: params.get('userLevel'),
        target: params.get('target'),
    };
};

export default async function (req: NextApiRequest, res: NextApiResponse) {
    const channel = parseNightbotChannel(
        req.headers['nightbot-channel'] as string
    );
    
    const user = parseNightbotUser(req.headers['nightbot-user'] as string);

    let t = 'not found';
    for(let key in req.headers)
    {
        let v = req.headers[key] as string;

        let ndx = v.indexOf('target');
        if(ndx >= 0)
        {
            t = `target found in ${key} at ${ndx}`;
        }
        let params = new URLSearchParams(v);

        if(params.get('target') != null)
        {
            t = `target found in ${key}, v = ${v}`;
        }
    }
    
    if(req.query)
    {
        if(typeof(req.query) == 'object')
        {
            if(req.query['target'])
            {
                t = `target found in req.query.target = ${req.query['target']}`;
            }
            else 
            {
                let params = new URLSearchParams(req.query);
                if(params.get('target'))
                {
                    t = `target found in params(req.query) = ${params.get('target')}`;
                }
                else
                {
                    t = 'req.query was an object but target not found'   
                }
            }
        }
        else if(typeof(req.query) == 'string')
        {
            t = 'req.query was a string = ' + req.query;
        }
    }

    res
        .status(200)
        .send(
            `Hello! Your username is ${user.displayName} and the current channel is ${channel.displayName}. result = ${t}`
        );
};
