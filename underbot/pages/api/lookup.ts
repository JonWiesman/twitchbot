// lookup.ts

import { NextApiRequest, NextApiResponse } from 'next';
import path from 'path';
import { promises as fs} from 'fs';

export default async function (req: NextApiRequest, res: NextApiResponse) {

    let homeGameHistory = [];
    
    let resText = 'Usage: lookup [name].';

    const jsonDirectory = path.join(process.cwd(), 'json');
    const fileContents = await fs.readFile(jsonDirectory + '/homegamewinners.json', 'utf8');

    
    if(fileContents)
    {
        if(fileContents.length == 0)
        {
            resText = fileContents;
        }
        else
        {
            console.log(`fileContents = ${fileContents.substring(0, 100)}`);
            let doc = JSON.parse(fileContents);
            console.log(`returned from parsing fileContents!`);
            if(doc && doc.db)
            {
                homeGameHistory = doc.db;
            }
        }
    }

    if(req.query.target)
    {
        let target = req.query.target as string;
        if(target[0] == '@')
        {
            // strip the @
            target = target.substring(1);
        }

        fetch(`https://twitchbot-phi.vercel.app/api/getnick?target=${target}`).then(function(response){
            return response.json();
        }).then(function(myjson) {

            let globalSn = '';
            let twitchSn = '';

            if(myjson.db && myjson.db.length > 0)
            {
                globalSn = myjson.db[0].globalName;
                twitchSn = myjson.db[0].twitchName;
            }
            else
            {
                console.log(`myjson = ${myjson}`);
                let xp = JSON.parse(myjson);
                if(xp && xp.db && xp.db.length > 0)
                {
                    globalSn = xp.db[0].globalName;
                    twitchSn = xp.db[0].twitchName;
                }
            }
            
            let key = target.toLowerCase();

            if(key == "globalpokerlive")
            {
                resText = 'GlobalPokerLive is The Undisputed KING of all social media! But, alas, not the Live Stream game.';
                target = '';
            }
            else if(key == "supersapi")
            {
                resText = 'Supersapi is on vacation too often to actually play poker.';
                target = '';
            }
            else
            {
                resText = `No GlobalPoker-to-Twitch match found for ${target}. `;

                if(globalSn.length > 0)
                {
                    resText = `Twitch user ${twitchSn} is Global Poker player ${globalSn}. `;
                }
            }

            let historyKey = globalSn.length > 0 ? globalSn : target;
            if(historyKey.length > 0)
            {
                let lastDate = '';
                let wins = 0;
                let lastWinDate = '';
                let lastEntrants = 0;
                let lastStreamer = '';
                let totalStreams = 0;
                let totalEntrants = 0;
                let streamWins = 0;
                for(let i = 0; i < homeGameHistory.length; i++)
                {
                    let info = homeGameHistory[i];
                    if(info.date.length == 0)
                    {
                        break;
                    }
                    lastDate = info.date;
                    let win = info.winner.toLowerCase() == historyKey.toLowerCase();
                    if(win)
                    {
                        wins++;
                        lastWinDate = info.date;
                        lastEntrants = info.entrants;
                        lastStreamer = info.streamer;
                    }
                    if(info.streamer.toLowerCase() == twitchSn.toLowerCase())
                    {
                        totalStreams++;
                        totalEntrants += info.entrants;
                        if(win)
                        {
                            streamWins++;
                        }
                    }
                }
                let addDate = true;
                if(historyKey.toLowerCase() == 'chawell')
                {
                    resText += `Just another player. ${wins} wins. BFD. Last time: ${lastWinDate}, ${lastEntrants} entrants and ${lastStreamer} was streaming. `;
                }
                else if(historyKey.toLowerCase() == "lily whites")
                {
                    resText += `Winterwren is the undisputed MVP of the Global Poker Live channel. All Hail Winterwren! She has ${wins} win(s). Last time: ${lastWinDate}, ${lastEntrants} entrants and ${lastStreamer} was streaming. `;
                }
                else if(historyKey.toLowerCase() == 'underdogca')
                {
                    resText += `Rattlesnake Main Event High Champion UnderdogCA has ${wins} Home Game wins. Last time: ${lastWinDate}, ${lastEntrants} entrants and ${lastStreamer} was streaming. `;
                }
                else if(wins > 0)
                {
                    resText += `${historyKey} has won the Home Game ${wins} time(s). Their last win was ${lastWinDate}, there were ${lastEntrants} entrants and ${lastStreamer} was streaming. `;
                }
                if(totalStreams > 0)
                {
                    resText += `GlobalPoker Streamer stats: ${totalStreams}, avg entrants: ${(totalEntrants/totalStreams).toFixed(1)}, streamer wins: ${streamWins}. `;
                }
                if(addDate)
                {
                    resText += `Lookup tool last updated: ${lastDate}. `;
                }
            }
            if(globalSn.length >= 0)
            {
                res
                .status(200)
                .send(
                    `${resText}`
                );
            }
            return;
        });
    }
    else
    {
        res
        .status(200)
        .send(
            `${resText}`
        );
    }
};
