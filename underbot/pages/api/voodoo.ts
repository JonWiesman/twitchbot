// voodoo.ts

import { NextApiRequest, NextApiResponse } from 'next';
import { URLSearchParams } from 'url';


const parseNightbotUser = (userParams: string) => {
    const params = new URLSearchParams(userParams);
  
    return {
        name: params.get('name'),
        displayName: params.get('displayName'),
        provider: params.get('provider'),
        providerId: params.get('providerId'),
        userLevel: params.get('userLevel'),
    };
};

export default async function (req: NextApiRequest, res: NextApiResponse) {
    const user = parseNightbotUser(req.headers['nightbot-user'] as string);
    let target = req.query.target;


    let outcomes = [
        ` and ${target} gets dealt KK. An Ace comes on the flop!`,
        ` and ${target} feels a minor itch.`,
        ` and ${target} feels a sharp pain!`,
        ` but nothing happens. ${target} yawns.`,
        `. Shouldn't ${user.displayName} stop playing with dolls?`,
        `. The doll grabs the pin and stabs ${user.displayName} right back!`,
        ` but ${target} likes it. ${user.displayName} puts the doll down.`,
        ` and pulls it out and the doll explodes. Wait, that was a grenade.`,
        ` and ${target} stone cold bubbles The Scrimmage.`,
        ` and yells, "It's not a doll, it's an action figure!"`,
        ` and ${target} gets dealt two red fives. JWK24 tells a story about winning a tournament with red fives and one chip.`,
        ` and ${target}'s Pandora station switches to "TheLango Sings Christmas Carols, Volume 3".`,
        ` and ${target}'s cards are replaced by two wet bar napkins vs. ${user.displayName}'s KK. They chop.`,
        ` and ${target} busts and forgets to rebuy in the Live Stream Game.`,
        ` and ${target} gets a hearty fist bump from Skybuxs.`,
        ` at the same time ${target} sticks a pin in a ${user.displayName} doll. The dolls are like, wtf.`,
        ` and Pontamous yells "BOOBS!" That had nothing to do with the pin or the doll or anything. She just does that. <3`,
        `. Winterwren asks ${target} if they would like to be in the lookup database because she is awesome.`,
        ` and Conti records that ${target} is ${user.displayName}'s permanent horse in all future horse races.`,
        ` and Nightbot replaces ${target}'s name with "Nightbot" on all future wheel spins.`,
        `. Chawell reminds everyone that he has the most home game wins. The doll grabs the pin from ${user.displayName} and stabs itself.`,
    ];

    let resText = `${user.displayName} sticks a pin in a ${target} doll`;
    let roll = Math.floor(Math.random() * outcomes.length);

    resText += outcomes[roll];

    res
        .status(200)
        .send(
            resText
        );
};
